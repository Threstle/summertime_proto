# Summertime
Just a prototype.

## Installation
You need to have ruby and compass installed globally.
First, go to directory project, then :
	
	npm install
	bower install

## Run app

	gulp buildlite

Builds template, css and js

	gulp build

Builds complete project (compress and copy imgs in /dist)

	gulp dev

Builds with buildlite and watch for any change in js/css/template

## Notes

To go the next scene press the down arrow. It will change after 5s.