<!DOCTYPE html>
<html>
	<head>
		<title>A Short Journey</title>

        <link rel="shortcut icon" href="src/img/0_FAVICON_64x64.png">

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta property="fb:app_id"          content="204459056567109" /> 
        <meta property="og:url"           content="http://ashortjourney.com" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="A Short Journey" />
        <meta property="og:description"   content="Take a deep breath to begin 2016 and enjoy the ride" />
        <meta property="og:image"         content="http://ashortjourney.com/dist/img/0_SHARE_1200x900.jpg" />
        
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="A Short Journey" />
        <meta name="twitter:description" content="Take a deep breath to begin 2016 and enjoy the ride" />
        <meta name="twitter:image" content="http://ashortjourney.com/dist/img/0_SHARE_1200x900.jpg" />

		<link rel="stylesheet" href="dist/stylesheets/screen.css">

        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/raphael/raphael-min.js"></script>

		<script src="vendor/dat.gui.min.js"></script>

        <meta name="viewport" content="user-scalable=no, shrink-to-fit=0, width=device-width, initial-scale=1, maximum-scale=1">
        <style>
            head, body{
                width:100%;
                height:100%;
                overflow: hidden;
                top:0;
                left:0;
                margin:0;
                padding:0;
                background-color:#EEEEEE;
            }
        </style>

        <script src="vendor/three/three.js"></script>
        <script src="vendor/three/BinaryLoader.js"></script>
        <script src="vendor/modernizr-custom.js"></script>

        <script src="vendor/gsap/TweenMax.js"></script>
        <script src="vendor/gsap/easing/EasePack.js"></script>


        <script src="js/4_alfred/3d/ShaderLoader.js"></script>
        <!--<script src="js/4_alfred/3d/background.js"></script>-->
        <script src="js/4_alfred/3d/textures.js"></script>
        <script src="js/4_alfred/3d/materials.js"></script>
        <script src="js/4_alfred/3d/stage3d.js"></script>
        <script src="js/4_alfred/3d/models.js"></script>

        <script src="js/4_alfred/3d/Transition.js"></script>

        <script src="js/4_alfred/utils.js"></script>


        <script src="js/4_alfred/scenes/Scene.js"></script>
        <script src="js/4_alfred/scenes/S1_desk.js"></script>
        <script src="js/4_alfred/scenes/S2_bag.js"></script>
        <script src="js/4_alfred/scenes/S3_depart.js"></script>
        <script src="js/4_alfred/scenes/S4_paysage.js"></script>
        <script src="js/4_alfred/scenes/S5_jumelles.js"></script>
        <script src="js/4_alfred/scenes/S6_beach.js"></script>
        <script src="js/4_alfred/scenes/S7_room.js"></script>


        <!-- objects -->
        <script src="js/4_alfred/scenes/objects/animation_perso.js"></script>
        <script src="js/4_alfred/scenes/objects/Bag.js"></script>
        <script src="js/4_alfred/scenes/objects/Beach.js"></script>
        <script src="js/4_alfred/scenes/objects/Camera.js"></script>
        <script src="js/4_alfred/scenes/objects/Car.js"></script>
        <script src="js/4_alfred/scenes/objects/CarBackground.js"></script>
        <script src="js/4_alfred/scenes/objects/Clock.js"></script>
        <script src="js/4_alfred/scenes/objects/Desk.js"></script>
        <script src="js/4_alfred/scenes/objects/Jumelles.js"></script>
        <script src="js/4_alfred/scenes/objects/Landscape.js"></script>
        <script src="js/4_alfred/scenes/objects/LandscapeBackground.js"></script>
        <script src="js/4_alfred/scenes/objects/Pot.js"></script>
        <script src="js/4_alfred/scenes/objects/Road.js"></script>
        <script src="js/4_alfred/scenes/objects/Room.js"></script>
        <script src="js/4_alfred/scenes/objects/Room0.js"></script>

        <script src="js/4_alfred/Alfred.js"></script>


	</head>
	<body>
       
        <?php include 'templates/ui.html'; ?>
        <?php include 'templates/flash.html'; ?>
        <?php include 'templates/intro.html'; ?>
        <?php include 'templates/credits.html'; ?>
        <?php include 'templates/end.html'; ?>
        <?php include 'templates/postcard.html'; ?>
        <?php include 'templates/jumelles.html'; ?>
        <?php include 'templates/error.html'; ?>
        <?php include 'templates/errorMobile.html'; ?>
        <?php include 'templates/old.html'; ?>

        <!--build
        <script src="dist/js/all.js"></script>
        //-->

        <script>

</script>

        <!--prototypage
        -->
        <script src="bower_components/raphael/raphael-min.js"></script>
        <script src="js/1_apis/4_audio.js"></script>
        <script src="js/1_apis/5_ui.js"></script>
        <script src="js/1_apis/6_nav.js"></script>
		<script src="js/5_main/main.js"></script>
	</body>

</html>