var Mouse = function(doc,that){
	var _this = this;

	var _this = this;

	_this.doc = doc;
	_this.doc.onmouseup = _this.endDrag;
	_this.objects3D = [];
	_this.parent = that;

	_this.doc.onmousedown = function(e){

		_this.beginDrag(e);
	}

	_this.doc.onmousemove = function(e){

		_this.moveMouse(e);
		_this.moveDrag(e);
	}

	_this.doc.onmouseup = function(e){

		_this.endDrag(e);
	}
};

Mouse.prototype.beginDrag = function(e) {
	var _this = this;

	_this.isDown = true;
	
	_this.startPosX = e.pageX;
	_this.startPosY = e.pageY;




};

Mouse.prototype.moveMouse = function(e){
	var _this = this;

	_this.parent.propagateMouseMove(e);
	//this.parent.rotateCamera((e.pageX-(window.innerWidth/2)),(e.pageY-(window.innerHeight/2)));
};

Mouse.prototype.moveDrag = function(e) {
	var _this = this;

	if(_this.isDown){
		
		_this.dragAmountX = (_this.startPosX-e.pageX);
		_this.dragAmountY = (_this.startPosY-e.pageY);
		_this.parent.propagateDrag(_this.dragAmountX,_this.dragAmountY);
	}


};

Mouse.prototype.endDrag = function(e) {
	var _this = this;

	_this.isDown = false;
	_this.parent.endDrag();
	
};