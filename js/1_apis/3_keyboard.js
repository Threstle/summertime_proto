var Keyboard = function(doc,that){
	var _this = this;

	_this.doc = doc;
	_this.parent = that;
	_this.power = 0;
	_this.loadCoef = 0;

	_this.doc.onkeydown = function(e){
		_this.beginPress(e);
		_this.press(e);
	}

	_this.doc.onkeyup = function(e){
		_this.stopPress(e);
	}

	_this.load();
};

Keyboard.prototype.press = function(e) {
	var _this = this;
	
	_this.parent.propagateKeyPress(e);
};

Keyboard.prototype.beginPress = function(e) {
	var _this = this;
	
	_this.parent.propagateKeyBegin(e);
	_this.loadCoef = 0.5;
};

Keyboard.prototype.stopPress = function(e) {
	var _this = this;
	
	_this.parent.propagateKeyStop(e);
	_this.loadCoef = -1;
};

Keyboard.prototype.load = function(e) {
	var _this = this;

	_this.parent.propagateKey(this.power,e);
	_this.timer = setTimeout(function () {

    	_this.power+=_this.loadCoef;
    	if(_this.power < 0)_this.power = 0;
    	_this.load();

	}, 100);

};