

/*
var UI = function(width,height){
	var _this = this;

	_this.size = {w:width,h:height};

	_this.c = {ra:0.25,r:40};
	_this.g = {ra:0.75,r:15};

	_this.paper = Raphael(0, 0, width, height);

	_this.draw(0,0,_this.size.w,_this.size.h);
}

UI.prototype.draw = function(x,y,w,h) {
	var _this = this;

	_this.ls = {x:x,y:y,w:w,h:h};

	var line = _this.paper.path( "M"+_this.ls.x+","+_this.ls.y+" L"+_this.ls.w+","+_this.ls.h).attr({
		'stroke-width':'5',
		'stroke':"white",
		'stroke-dasharray': '. ',
		'stroke-linecap': 'round'
	});

	_this.cursor = _this.drawCircle(_this.c.ra,_this.c.r);
	_this.goal = _this.drawCircle(_this.g.ra,_this.g.r);


};

UI.prototype.drawCircle = function(ra,r) {
	var _this = this;

	var pos = _this.getPosAlongPath(ra);

	return _this.paper.circle(pos.x,pos.y,r).attr({
		'fill':'white',
		'stroke':'transparent'
	});
};

UI.prototype.getPosAlongPath = function(ra){
	var _this = this;

	var xlen = _this.ls.w - _this.ls.x;
	var ylen = _this.ls.h - _this.ls.y;

	var lengthLine = Math.sqrt(Math.pow(xlen,2) + Math.pow(ylen,2));

	var ratio = ra;

	var smallerXLen = xlen * ratio;
	var smallerYLen = ylen * ratio;

	var smallerX = _this.ls.x + smallerXLen;
	var smallerY = _this.ls.y + smallerYLen;

	return {x:smallerX,y:smallerY};

}

UI.prototype.animateCursor = function(ra){
	var _this = this;

	ra = (ra*(_this.g.ra-_this.c.ra))+_this.c.ra;
	var newPos = _this.getPosAlongPath(ra);
	_this.cursor.attr({
		cx:newPos.x,
		cy:newPos.y
	});
}

UI.prototype.resize = function(width,height) {
	var _this = this;

	_this.paper.setSize(width,height);
};
//*/