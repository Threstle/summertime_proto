var Object3D = function(scene,tab,pos,scale,rot,options,param){
	var _this = this;
	_this.three = THREE;
	_this.animations = [];
	_this.keyPower = 0;

	_this.scene = scene;
	_this.tab = tab;
	_this.pos = pos;
	_this.rot = rot;
	_this.scale = scale;
	// The way options and param are distincts is not logical, I'll have to rethink it
	_this.options = options;
	_this.param = param;

	_this.isDraggable = false;
	_this.isAnimated = false;

	_this.createObject();
};

Object3D.prototype.addObject= function(object) {
	var _this = this;
	_this.instance = object;
	_this.setParam();
	
	if(_this.param && _this.param.root){
		_this.param.root.instance.add(_this.instance);
	}
	else _this.scene.add(_this.instance);
	
	_this.tab[_this.tab.length] = _this;
};

Object3D.prototype.setParam = function() {
	var _this = this;

    if(_this.param && _this.param.isDraggable)_this.isDraggable = _this.param.isDraggable;
    if(_this.param && _this.param.sound!=null){_this.sound = _this.param.sound;}
    if(_this.param && _this.param.isAnimated)_this.isAnimated = _this.param.isAnimated;
    if(_this.param && _this.param.isHollow)_this.isHollow = _this.param.isHollow;
    if(_this.param && _this.param.isPhysic)_this.isPhysic = _this.param.isPhysic;
    if(_this.param && _this.param.isShakable)_this.isShakable = _this.param.isShakable;
    if(_this.param && _this.param.isTransparent){
    	_this.instance.material.transparent = _this.param.isTransparent;
    	_this.instance.material.opacity = 0;
    }
};




Object3D.prototype.createBackground = function(x,y,z,texture,scene,parallax,scale){

};



Object3D.prototype.loadKey = function(power){
	var _this = this;
	_this.keyPower = power/10;
};

Object3D.prototype.rotate = function(amountX,amountY){
	var _this = this;
	if(_this.isDraggable){
		_this.instance.position.x -= amountX/2;
		_this.instance.position.y += amountY/2;
	}

};



