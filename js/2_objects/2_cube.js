var Cube = function(scene,tab,pos,scale,rot,options,param){
	var _this = this;
	Object3D.call(_this,scene,tab,pos,scale,rot,options,param);

};

Cube.prototype = Object.create(Object3D.prototype);
Cube.prototype.constructor = Object3D;

Cube.prototype.createObject = function() {
	var _this = this;
   	var material = null;
		if(_this.options && _this.options.texture){
			var texture = THREE.ImageUtils.loadTexture( _this.options.texture );
			texture.repeat.set( 1, 1 );
			texture.wrapS = THREE.RepeatWrapping;
			texture.wrapT = THREE.RepeatWrapping;
			texture.minFilter = THREE.NearestFilter;
			material = new THREE.MeshBasicMaterial( {transparent:true,map:texture,lightmap:texture} );
		}
		else if(_this.options && _this.options.color){
			material = new THREE.MeshBasicMaterial( {transparent:true,color:_this.options.color} );
		}
		else{
			material = new THREE.MeshPhongMaterial();
		}

		if(_this.options.video){
				var video = document.getElementById( 'video_laptop' );
				var texture = new THREE.VideoTexture( video );
				texture.minFilter = THREE.LinearFilter;
				texture.magFilter = THREE.LinearFilter;
				texture.format = THREE.RGBFormat;
				material = new THREE.MeshBasicMaterial( {map:texture} );
		}

		var cube = new THREE.Mesh( new THREE.BoxGeometry( _this.scale.x, _this.scale.y, _this.scale.z, 10,10,10), material);
		cube.geometry.verticesNeedUpdate = true;
		cube.castShadow = true;
		cube.receiveShadow = true;
		// _this.instance.rotation.x = -1;
		cube.rotation.x = _this.rot.x;
		cube.rotation.y = _this.rot.y;
		cube.rotation.z = _this.rot.z;
		cube.position.x = _this.pos.x;
		cube.position.y = _this.pos.y;
		cube.position.z = _this.pos.z;
		cube.tag = 1;
		
		_this.addObject(cube);

};

