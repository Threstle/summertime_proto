var Cylinder = function(scene,tab,pos,scale,rot,options,param){
	var _this = this;
	Object3D.call(_this,scene,tab,pos,scale,rot,options,param);

};

Cylinder.prototype = Object.create(Object3D.prototype);
Cylinder.prototype.constructor = Object3D;

Cylinder.prototype.createObject = function() {
	var _this = this;
	var material = null;
	if(_this.options && _this.options.texture){
		var texture = THREE.ImageUtils.loadTexture( _this.options.texture );
		texture.repeat.set( 1, 1 );
		texture.wrapS = THREE.RepeatWrapping;
		texture.wrapT = THREE.RepeatWrapping;
		texture.minFilter = THREE.NearestFilter;
		material = new THREE.MeshBasicMaterial( {transparent:true,map:texture} );
	}
	else if(_this.options && _this.options.color){
		material = new THREE.MeshPhongMaterial( {transparent:true,color:_this.options.color} );
	}
	else{
		material = new THREE.MeshPhongMaterial({transparent:true});
	}
	material.transparent = true;
	material.opacity = 0;
	_this.isShakable = _this.options.isShakable;
	if(_this.options.isHollow){
		var cylindre = new Physijs.ConcaveMesh( new THREE.CylinderGeometry( _this.scale.x, _this.scale.y, _this.scale.z, 8, 1, true,0,Math.PI*2), material);
	}
	else{
		var cylindre = new Physijs.CylinderMesh( new THREE.CylinderGeometry( _this.scale.x, _this.scale.y, _this.scale.z, 8, 1, true,0,Math.PI*2), material);
	}
	
	cylindre.geometry.verticesNeedUpdate = true;
	cylindre.castShadow = true;
	cylindre.receiveShadow = true;
	cylindre.geometry.openEnded = true;
	cylindre.material.side = THREE.DoubleSide;
	cylindre.rotation.x = _this.rot.x;
	cylindre.rotation.y = _this.rot.y;
	cylindre.rotation.z = _this.rot.z;
	cylindre.position.x = _this.pos.x;
	cylindre.position.y = _this.pos.y;
	cylindre.position.z = _this.pos.z;
	cylindre.tag = 1;
	
	_this.addObject(cylindre);

	setInterval(function(){
		
	},1000)



};

Cylinder.prototype.shake = function(power) {
	var _this = this;
		if(_this.sound)_this.sound.play();
		var force = new THREE.Vector3(10*Math.random(),0.1*Math.random(),10*Math.random());
		var pos = new THREE.Vector3(-1,0,-1);
		_this.instance.applyImpulse(force,pos);
};

Cylinder.prototype.setParam = function() {
    
};