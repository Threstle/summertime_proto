var Collada = function(scene,tab,objet,pos,scale,rot,param){
	var _this = this;

	_this.objet = objet;
	_this.ratio = 0;
	Object3D.call(_this,scene,tab,pos,scale,rot,null,param);

};

Collada.prototype = Object.create(Object3D.prototype);
Collada.prototype.constructor = Object3D;

//Load collada object and set it in the scene
Collada.prototype.createObject = function() {
	var _this = this;

	_this.loader = new THREE.ColladaLoader();
	_this.loader.options.convertUpAxis = true;
	_this.loader.load(_this.objet+"object.dae", function ( collada ) {

		dae = collada.scene;

		_this.loadMaterial(_this.objet,dae);
		_this.loadAnimations(collada);

		dae.scale.set(_this.scale,_this.scale,_this.scale);
		dae.rotation.set(_this.rot.x,_this.rot.y,_this.rot.z);
				dae.position.set(_this.pos.x,_this.pos.y,_this.pos.z);
		dae.castShadow = true;
		dae.updateMatrix();		

		_this.addObject(dae);
	});

};

// Load collada object animations
Collada.prototype.loadAnimations = function(collada) {
	var _this = this;

	var animations = collada.animations;

		animations.forEach(function(anim){			 
				 	if(anim.length===0)return;
				 	 var kfAnimation = new THREE.KeyFrameAnimation( anim );
				 	 kfAnimation.timeScale = 0;

				 	for ( var h = 0, hl = kfAnimation.hierarchy.length; h < hl; h++ ) {
						var keys = kfAnimation.data.hierarchy[ h ].keys;
						var sids = kfAnimation.data.hierarchy[ h ].sids;
						var obj = kfAnimation.hierarchy[ h ];
						if ( keys.length && sids ) {
							for ( var s = 0; s < sids.length; s++ ) {
								var sid = sids[ s ];
								var next = kfAnimation.getNextKeyWith( sid, h, 0 );
								if ( next ) next.apply( sid );
							}
							obj.matrixAutoUpdate = false;
							kfAnimation.data.hierarchy[ h ].node.updateMatrix();
							obj.matrixWorldNeedsUpdate = true;
						}
					}
					kfAnimation.loop = false;
					kfAnimation.startTime = 0;
					kfAnimation.play();
					_this.animations.push(kfAnimation);


				 });
	
};

// Load collada object material by loading a json containing the textures
Collada.prototype.loadMaterial = function(obj,dae) {
	var _this = _this;
	var index = 0;
	$.getJSON(obj+"texture.json", function(json) {

		dae.traverse( function ( child ) {
			index++;
			
			if(json[index]){
				var texture = THREE.ImageUtils.loadTexture(obj+json[index]);
				texture.minFilter = THREE.NearestFilter;
				child.material = new THREE.MeshBasicMaterial( { map: texture, side:THREE.DoubleSide } );
			}

		});

    });
};

// Animate the collada object
Collada.prototype.animate = function(timestamp,lastTimestamp) {

	var _this = this;
	_this.animations.forEach(function(anim){
		 if(anim.currentTime==anim.data.length){anim.stop();anim.play();}
		 else{}

		if(anim!==null&&anim.data.length!==0){
			anim.timeScale = 0.2;

			var frameTime = (timestamp - lastTimestamp)*_this.ratio;
			anim.update(frameTime);

		}
		
	});

};