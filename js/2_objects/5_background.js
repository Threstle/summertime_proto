var Background = function(scene,tab,parallax,pos,scale,rot,options,param){
	this.parallax = parallax;
	Object3D.call(this,scene,tab,pos,scale,rot,options,param);
	this.ratio = 0;
	
};

Background.prototype = Object.create(Object3D.prototype);
Background.prototype.constructor = Object3D;

// Load sprite on a single plane. Can play video too (though it's better to use Cube for that)
Background.prototype.createObject = function() {

   	var onRenderFcts = [];

		
	if(this.options.video){
				var video = document.getElementById( 'video_laptop' );
				var texture = new THREE.VideoTexture( video );
				texture.minFilter = THREE.LinearFilter;
				texture.magFilter = THREE.LinearFilter;
				texture.format = THREE.RGBFormat;
	}
	else{
		var texture = THREE.ImageUtils.loadTexture( this.options.texture );
	
		if(this.options.ratio){
			texture.repeat.set( this.options.ratio.x, this.options.ratio.y );
		}

		texture.wrapS = THREE.RepeatWrapping;
		texture.wrapT = THREE.RepeatWrapping;
		texture.minFilter = THREE.NearestFilter;
	}
	



	texture.minFilter = THREE.NearestFilter;

	var geometry = new THREE.PlaneBufferGeometry( this.scale.x, this.scale.y, this.scale.z );
	var material = new THREE.MeshBasicMaterial( {transparent:true,map:texture} );
	
	if(!this.isPhysic){
		var plane = new Physijs.BoxMesh( geometry, material, 0);
	}
	else{
		var plane = new THREE.Mesh( geometry, material );
	}
	plane.position.set(this.pos.x,this.pos.y,this.pos.z);
	plane.rotation.set(this.rot.x,this.rot.y,this.rot.z);

	plane.tag = 0;
	plane.receiveShadow = true;
	
	this.texture = texture;
	this.updateOffset(plane.material.map);
	this.addObject(plane);



};

// Update offset to animate sprite (used to create parallax)
Background.prototype.updateOffset = function(texture){
	if(this.parallax!==null && !this.options.video){

		setTimeout(function(){
			texture.offset.x += this.parallax.x*this.ratio; // 0.0 - 1.0
			texture.offset.y += this.parallax.y*this.ratio; // 0.0 - 1.0
			this.updateOffset(texture);
		}.bind(this),10);
	}

};
