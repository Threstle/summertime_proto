var ThreeScene = function(){
	var _this = this;


	
	// Create and setup physic scene
	_this.scene = new Physijs.Scene();
	_this.scene.setGravity(0,-10,0);


	_this.lastTimestamp = 0;
	_this.objects = [];
	_this.isLeaving = false;
	_this.limitCam = {xMin:-9999,yMin:-9999,xMax:9999,yMax:9999};

	_this.mousePos = {x:0,y:0,z:0};
	_this.ecart = {x:0,y:0};
	_this.mouseRatio = 1;

	// Setup Three scene
	_this.camera = new THREE.PerspectiveCamera( 40, window.innerWidth/window.innerHeight, 0.1, 1000 );
	_this.renderer = new THREE.WebGLRenderer({antialias:true});
	_this.renderer.setSize( window.innerWidth, window.innerHeight );
	_this.renderer.setClearColor( 0xffffff, 1);
	_this.renderer.shadowMapEnabled = true;
	_this.renderer.shadowMapType = THREE.PCFSoftShadowMap;
	_this.renderer.sortObjects = false;
	_this.renderer.preserveDrawingBuffer = true;

	// Set distance camera
	_this.camera.position.z = 10;

	// Create dom wrapper
	_this.wrapper = document.createElement('div');
    _this.wrapper.className = "wrapper";
	_this.wrapper.appendChild(_this.renderer.domElement);
	document.body.appendChild(_this.wrapper);
	_this.setUI();
	
	// Resize canvas with window
	this.resizeCallback = function(){

		_this.camera.aspect = window.innerWidth / window.innerHeight;
	    _this.camera.updateProjectionMatrix();
	    _this.renderer.setSize( window.innerWidth, window.innerHeight );

	}

	window.addEventListener( 'resize', this.resizeCallback, false );


	_this.render(this.lastTimestamp);
	
};

// add 3D object to object tab
ThreeScene.prototype.addObject = function(object) {
	this.objects[this.objects.length]=object;
};

// render the scene, update ui and simulate physics
ThreeScene.prototype.render = function (timestamp) {
	var _this = this;

	_this.requestId = requestAnimationFrame(this.render.bind(this));
	_this.renderer.render(_this.scene, _this.camera);
	TWEEN.update();
	_this.scene.simulate();
	_this.update();
	_this.updateUI();
	
	_this.objects.forEach(function(o){
		if(o.isAnimated)o.animate(timestamp,_this.lastTimestamp);
	});
	
	_this.lastTimestamp = timestamp;

};

// Update function 
ThreeScene.prototype.update = function() {
	var _this = this;

	// Recenter camera if going to next scene
	if(_this.isLeaving){
		_this.moveCamera();
	}
};

// Get object from _this.objects tab with 3D object
ThreeScene.prototype.getObject = function(objectInstance){
	var _this = this;

	for(var i = 0; i < _this.objects.length;i++){
		if(objectInstance == _this.objects[i].instance)return _this.objects[i];
	}
};

// Move camera according to mouse and get object pointed by mouse (shake it if affected by physics)
ThreeScene.prototype.propagateMouseMove = function(event) {
	var _this = this;

	var posMouseX = -(event.pageX-(window.innerWidth/2));
	var posMouseY = (event.pageY-(window.innerHeight/2));

	_this.mouse = {x:posMouseX/500,y:posMouseY/500};
	
	_this.moveCamera();

	var vector = new THREE.Vector3();
	var raycaster = new THREE.Raycaster();
	var dir = new THREE.Vector3();
	var objects = [];

	// Find object pointed by mouse by raycasting and matching with objects in _this.objects tab
 	vector.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1, 0.5 );

    vector.unproject( this.camera );

    raycaster.set(_this.camera.position, vector.sub(_this.camera.position ).normalize() );
        var intersects = raycaster.intersectObjects(_this.scene.children, true );

	      _this.scene.children.forEach(function(child){

	      	if(child.material){
	      		child.material.wireframe = false;
	      		
	      		if(intersects[0]){
	      			_this.mousePos.x = intersects[0].point.x;
	      			_this.mousePos.y = intersects[0].point.y;
	      			_this.mousePos.z = intersects[0].point.z;
	      			if(child == intersects[0].object){
	      				
	      				
	      				_this.hoverObject = _this.getObject(child);
	      				if(_this.hoverObject.isShakable)_this.hoverObject.shake();

	      			}
	      		}
	      		
	     	}
	      });
  	


};

// Move camera around with mouse
ThreeScene.prototype.moveCamera = function() {
	var _this = this;

	if(_this.mouse){

		var mouseCam = {x:_this.mouse.x*_this.mouseRatio,y:_this.mouse.y*_this.mouseRatio};
			
		if(mouseCam.x > _this.limitCam.xMax)mouseCam.x = _this.limitCam.xMax;
		if(mouseCam.y > _this.limitCam.yMax)mouseCam.y = _this.limitCam.yMax;
		if(mouseCam.x < _this.limitCam.xMin)mouseCam.x = _this.limitCam.xMin;
		if(mouseCam.y < _this.limitCam.yMin)mouseCam.y = _this.limitCam.yMin;	

		if(_this.cameraMove){
			if(_this.isLeaving)_this.mouse = {x:0,y:0};
			var xDistance = (mouseCam.x - _this.camera.position.x)+_this.ecart.x;
			var yDistance = (mouseCam.y - _this.camera.position.y)+_this.ecart.y;


			var distance = Math.sqrt(xDistance * xDistance + yDistance * yDistance);

			_this.camera.position.x += xDistance * 0.15;
			_this.camera.position.y += yDistance * 0.15;

			if(!_this.cameraPan)this.camera.lookAt(new THREE.Vector3(0,0,0));
		}
	}
};

// Methods to override
ThreeScene.prototype.setUI = function() {};
ThreeScene.prototype.updateUI = function() {};
ThreeScene.prototype.drag = function(amountX,amountY) {};
ThreeScene.prototype.endDrag = function(amountX,amountY) {};
ThreeScene.prototype.keyPress = function(e){};
ThreeScene.prototype.keyBegin = function(e){};
ThreeScene.prototype.keyStop = function(e){};
ThreeScene.prototype.loadKey = function(power){};