var IntroScene = function(canvas){
    var _this = this;
    ThreeScene.call(_this,canvas);

};
IntroScene.prototype = Object.create(ThreeScene.prototype);
IntroScene.prototype.constructor = IntroScene;


IntroScene.prototype.setUI = function() {
    var _this = this;

    _this.ui = document.createElement('div');
    _this.ui.id ="ui-intro";
    _this.ui.className = "ui";
    _this.wrapper.appendChild(_this.ui);

    $(_this.ui).html('<div id="intro-clouds"></div>'+
                                     '<div id="intro-sun"></div>'+
                                     '<div id="intro-clouds-1"></div>'+
                                     '<div id="intro-clouds-2"></div>'+
                                     '<div id="intro-text">'+
                                        '<img class="" src="'+Const.IMGS+'1_INTRO_logo_benoit_challand.png"></img>'+
                                        '<h3 class="">AND</h3>'+
                                        '<img class="" src="'+Const.IMGS+'1_INTRO_logo_cher_ami.png"></img>'+
                                     '</div>'
                                 );
    
};

IntroScene.prototype.updateUI = function() {
    var _this = this;

};
