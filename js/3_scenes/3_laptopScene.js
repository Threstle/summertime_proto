var LaptopScene = function(canvas){
    var _this = this;

    ThreeScene.call(_this,canvas);
    _this.cameraMove = true;
    _this.ecart.x = -7;
    _this.ecart.y = 10;
    _this.camera.position.z = 40;
   
    // Instantiate 3D objects
    _this.back = new Background(_this.scene,_this.objects,{x:0,y:0},{x:0,y:0,z:-25},{x:150,y:150,z:1},{x:0,y:0,z:0},{texture:Const.IMGS+'2_LAPTOP_Laptop_Wall.jpg',ratio:{x:1,y:1}});
    _this.floor = new Background(_this.scene,_this.objects,{x:0,y:0},{x:0,y:-10,z:-10},{x:100,y:100,z:1},{x:-Math.PI/2,y:0,z:0},{texture:Const.IMGS+'2_LAPTOP_Laptop_Floor.jpg',ratio:{x:1,y:1},isPhysic:true});
    _this.floorShadow = new Background(_this.scene,_this.objects,{x:0,y:0},{x:8.5,y:-9.9,z:6},{x:75,y:75,z:1},{x:-Math.PI/2,y:0,z:0},{texture:Const.IMGS+'2_LAPTOP_Laptop_Floor_shadow.png'});

    _this.laptopBottom = new Collada(_this.scene,_this.objects,'src/3Dobj/laptopBottom/',{x:0,y:-9,z:-14},0.25,{x:0,y:0,z:0},{isAnimated:true});
    _this.pivot = new Cube(_this.scene,_this.objects,{x:0,y:-9,z:-13.5},{x:23,y:34,z:3},{x:0,y:0,z:0},{},{isTransparent:true});
    _this.laptopTop = new Collada(_this.scene,_this.objects,'src/3Dobj/laptopTop/',{x:0,y:0,z:0},0.25,{x:-0.4,y:0,z:0},{isAnimated:true,root:_this.pivot});
    _this.screen = new Cube(_this.scene,_this.objects,{x:0,y:8,z:-0.6},{x:19,y:12,z:0.1},{x:0,y:0,z:0},{video:true},{root:_this.pivot});

    _this.calendar = new Collada(_this.scene,_this.objects,'src/3Dobj/calendar/',{x:-2,y:-2,z:-5},0.2,{x:0,y:0,z:0},{});

    _this.crayonBox = new Collada(_this.scene,_this.objects,'src/3Dobj/crayonPot/',{x:2,y:-12,z:-8},0.2,{x:0,y:0,z:0},{isPhysic:true});
    _this.crayonBoxCollider = new Cylinder(_this.scene,_this.objects,{x:17,y:-7,z:-14},{x:2.4,y:2.4,z:7},{x:0,y:0,z:0},{isHollow:true});
    _this.crayonBoxCollider.instance.mass = 0;

    _this.crayon1Collider = new Cylinder(_this.scene,_this.objects,{x:17,y:-1,z:-12},{x:0.5,y:0.01,z:10},{x:0.5,y:0,z:0},{isTransparent:true,isShakable:true},{sound:_this.stylo1});
    _this.crayon1 = new Collada(_this.scene,_this.objects,'src/3Dobj/crayon/',{x:0,y:0,z:0},0.2,{x:0,y:0,z:Math.PI/2},{root:_this.crayon1Collider});
    _this.crayon1Collider.mass = 15;

    _this.crayon2Collider = new Cylinder(_this.scene,_this.objects,{x:17.5,y:-1,z:-13},{x:0.5,y:0.01,z:10},{x:-0.1,y:0.2,z:0},{isTransparent:true,isShakable:true},{sound:_this.stylo1});
    _this.crayon2 = new Collada(_this.scene,_this.objects,'src/3Dobj/crayon/',{x:0,y:0,z:0},0.2,{x:0,y:0,z:Math.PI/2},{root:_this.crayon2Collider});
    _this.crayon2Collider.mass = 15;

    _this.crayon3Collider = new Cylinder(_this.scene,_this.objects,{x:17,y:-1,z:-13},{x:0.5,y:0.01,z:10},{x:-0.1,y:0.2,z:0},{isTransparent:true,isShakable:true},{sound:_this.stylo1});
    _this.crayon3 = new Collada(_this.scene,_this.objects,'src/3Dobj/crayon/',{x:0,y:0,z:0},0.2,{x:0,y:0,z:Math.PI/2},{root:_this.crayon3Collider});
    _this.crayon3Collider.mass = 15;


};
LaptopScene.prototype = Object.create(ThreeScene.prototype);
LaptopScene.prototype.constructor = LaptopScene;

// Instantiate UI
LaptopScene.prototype.setUI = function() {
    var _this = this;

    _this.ui = document.createElement('div');
    _this.ui.id ="ui-laptop";
    _this.ui.className = "ui";
    _this.wrapper.appendChild(_this.ui);
    $(_this.ui).html('<h1 class="instructions">CLOSE THE SCREEN</h1>'+
                    '<video id="video_laptop" autoplay muted loop style="display:none">'+
                        '<source src='+Const.IMGS+'2_LAPTOP_video-laptop.ogv type=\'video/ogg; codecs="theora, vorbis"\'>'+
                        '<source src='+Const.IMGS+'2_LAPTOP_video-laptop.mp4 type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>'+
                    '</video>');
    _this.instructions = $('.instructions');

    _this.stylo1 = $('#stylo1');
    _this.stylo2 = $('#stylo2');
    _this.stylo3 = $('#stylo3');
};

// Update UI
LaptopScene.prototype.updateUI = function() {
    var _this = this;

    _this.instructions.css('opacity',_this.angleRatio);
};

// Close/Open laptop
LaptopScene.prototype.drag = function(dragX,dragY) {
    var _this = this;

    var max = -0.48;
    var min = 1.615;
    if(_this.pivot && _this.hoverObject == _this.pivot)_this.draggedObject = _this.pivot;
    if(_this.pivot && _this.draggedObject == _this.pivot){ 
        _this.pivot.instance.rotation.x-=dragY/10000;
        _this.angleRatio = 1 - _this.pivot.instance.rotation.x-0.5;

        if(_this.pivot.instance.rotation.x<max){
            _this.pivot.instance.rotation.x=max;
           
        }
        if(_this.pivot.instance.rotation.x>min){
            _this.pivot.instance.rotation.x=min;
             window.summer.nextCanvas();
        }
    }
    else{

    }
    
};

LaptopScene.prototype.endDrag = function() {
    var _this = this;

    _this.draggedObject = null;
};