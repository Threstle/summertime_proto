var SuitCaseScene = function(canvas){
    var _this = this;

    ThreeScene.call(_this,canvas);
    _this.cameraMove = true;
    _this.cameraPan = false;
    _this.renderer.setClearColor( 0x74B4C0, 1 );
    _this.camera.position.z = 12;
    _this.ecart.y = 0;
    var light = new THREE.SpotLight( 0x757180 );
    light.position.set(100,100,100);
    light.castShadow = true;

    light.castShadow = true;
    light.shadowMapWidth = 4096;
    light.shadowMapHeight = 4096;

    var d = 50;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 3500;
    light.shadowBias = -0.0001;
    light.shadowDarkness = 0.45;

    _this.scene.add(light);


    _this.photoCamera = new Collada(_this.scene,_this.objects,'src/3Dobj/suitcase/camera/',{x:0.15,y:0.16,z:4.1},0.22,{x:Math.PI/2,y:-Math.PI/2.2,z:0});
    _this.bed = new Collada(_this.scene,_this.objects,'src/3Dobj/suitcase/bed/',{x:1.8,y:2,z:4},0.22,{x:Math.PI/2,y:-Math.PI/2.2,z:0});

    _this.jumelles = new Collada(_this.scene,_this.objects,'src/3Dobj/suitcase/jumelles/',{x:0.99,y:-1.3,z:4.28},0.20,{x:3.14/2,y:4.8,z:0});

    _this.sac = new Collada(_this.scene,_this.objects,'src/3Dobj/suitcase/sac/',{x:-0.1,y:0.2,z:4},0.22,{x:Math.PI/2,y:-Math.PI/2.2,z:0});
    _this.tshirt = new Collada(_this.scene,_this.objects,'src/3Dobj/suitcase/tshirt/',{x:2.5,y:0,z:5.5},0.22,{x:Math.PI/2,y:-Math.PI/1.2,z:0});
    _this.groundShadow = new Background(_this.scene,_this.objects,{x:0,y:0},{x:0,y:0,z:4.1},{x:10,y:10,z:1},{x:0,y:0,z:-Math.PI/2.2},{texture:'dist/img/3_SUITCASE_bag_floor.jpg',ratio:{x:1,y:1}});
    _this.ground = new Background(_this.scene,_this.objects,{x:0,y:0},{x:0,y:0,z:4},{x:30,y:30,z:1},{x:0,y:0,z:-Math.PI/2.2},{texture:'dist/img/3_SUITCASE_bag_floor.jpg',ratio:{x:1,y:1}});

    _this.sac.ratio = 1;

};
SuitCaseScene.prototype = Object.create(ThreeScene.prototype);
SuitCaseScene.prototype.constructor = SuitCaseScene;

SuitCaseScene.prototype.close = function(first_argument) {

};

SuitCaseScene.prototype.setUI = function() {
    var _this = this;

    _this.ui = document.createElement('div');
    _this.ui.id ="ui-suitcase";
    _this.ui.className = "ui";
    document.body.appendChild(_this.ui);
};

SuitCaseScene.prototype.drag = function() {
    var _this = this;
    
     if(_this.hoverObject && _this.hoverObject.isDraggable){

        _this.hoverObject.instance.position.set(_this.mousePos.x,_this.mousePos.y,_this.hoverObject.instance.position.z);

     }
};