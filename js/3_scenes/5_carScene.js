var CarScene = function(canvas){
  var _this = this;
	
	ThreeScene.call(_this,canvas);
    _this.cameraMove = true;
    _this.camera.position.z = 13;
    _this.limitCam.yMin = 0.1;

  _this.renderer.setClearColor( 0xe1f5fa, 1 );

      _this.car = new Collada(_this.scene,_this.objects,'src/3Dobj/austin/',{x:0,y:-1,z:5},0.035,{x:0,y:-Math.PI/2,z:0},{isAnimated:true});
      _this.ground = new Background(_this.scene,_this.objects,{x:0,y:0.01},{x:0,y:-1,z:7},{x:12,y:100,z:50},{x:-Math.PI/2,y:0,z:-Math.PI/2},{texture:'dist/img/4_CAR_car_ground.jpg',ratio:{x:1,y:10}});
     // _this.carShadow = new Background(_this.scene,_this.objects,{x:0,y:0},{x:-0.3,y:-0.99,z:2.1},{x:20,y:20,z:50},{x:-Math.PI/2,y:0,z:Math.PI/2},{texture:'dist/img/4_CAR_Mini_Austin_floor_shadow.png'});

      _this.sun = new Background(_this.scene,_this.objects,{x:0.002,y:0},{x:0,y:0,z:-6.2},{x:120,y:12,z:1},{x:0,y:0,z:0},{texture:'dist/img/4_CAR_car_sun.png',ratio:{x:10,y:1}});
      _this.clouds = new Background(_this.scene,_this.objects,{x:0.002,y:0},{x:0,y:0,z:-6.1},{x:120,y:12,z:1},{x:0,y:0,z:0},{texture:'dist/img/4_CAR_car_cloud_1.png',ratio:{x:10,y:1}});
      _this.clouds2 = new Background(_this.scene,_this.objects,{x:0.002,y:0},{x:0,y:0,z:-6.1},{x:120,y:12,z:1},{x:0,y:0,z:0},{texture:'dist/img/4_CAR_car_cloud_2.png',ratio:{x:10,y:1}});
     
      _this.mountain = new Background(_this.scene,_this.objects,{x:0.001,y:0},{x:0,y:-0.9,z:1.3},{x:120,y:12,z:1},{x:0,y:0,z:0},{texture:'dist/img/4_CAR_car_mountain.png',ratio:{x:10,y:1}});
      _this.mountain2 = new Background(_this.scene,_this.objects,{x:0.0016,y:0},{x:0,y:-0.9,z:2.1},{x:120,y:12,z:1},{x:0,y:0,z:0},{texture:'dist/img/4_CAR_car_mountain_2.png',ratio:{x:10,y:1}});
     
};

CarScene.prototype = Object.create(ThreeScene.prototype);
CarScene.prototype.constructor = CarScene;

CarScene.prototype.update = function(first_argument) {
  var _this = this;
      if(_this.clouds)_this.clouds.ratio = 0.3;
      if(_this.clouds2)_this.clouds2.ratio = 0.2;
};


CarScene.prototype.setUI = function() {
  var _this = this;
    _this.ui = document.createElement('div');
    _this.ui.id ="ui-car";
    _this.ui.className = "ui";
    _this.wrapper.appendChild(_this.ui);
    $(_this.ui).html('<div class="instructions">'+
                      '<h1>PRESS</h1>'+
                      '<h1>SPACE</h1>'+
                    '</div>'+
                    '<div id ="compteur">'+
                      '<img src="dist/img/4_CAR_compteur.png"></img>'+
                      '<img id="boosterAiguille" src="dist/img/4_CAR_aiguille_1.png"></img>'+
                      '<img src="dist/img/4_CAR_aiguille_2.png"></img>'+
                    '</div>'
                   );
    _this.instructions = $('.instructions');
    _this.boosterAiguille = $('#boosterAiguille')
};

// Update car speed counter
CarScene.prototype.updateUI = function() {
  var _this = this;

    _this.boosterAiguille.css('transform',"rotate("+(_this.angleAiguille-25)+"deg)");

    if(_this.angleAiguille > 80)_this.instructions.fadeOut();
    if(_this.angleAiguille > 170 && !_this.flagCountdown)_this.launchCountdown();
    if(_this.angleAiguille < 170 && _this.flagCountdown)_this.cancelCountdown();
};

//  Accelerate car animation and background paralax if key is pressed
CarScene.prototype.loadKey = function(power) {
  var _this = this;
  power/=20+1;
  power+=0.5+Math.random()*(0.01);
   var max = Math.random()/20 + 2.2;
   if(power > max)power = max;

  _this.angleAiguille = power*100;

      if(_this.clouds)_this.clouds.ratio = 0.3+ 1*power;
      if(_this.clouds2)_this.clouds2.ratio = 0.2+ 1*power;
      if(_this.mountain)_this.mountain.ratio = 1*power;
      if(_this.mountain2)_this.mountain2.ratio = 1*power;
      if(_this.ground)_this.ground.ratio = 1*power;
      _this.car.ratio = 0.006*power;
      

};

// Launch countdown to next scene if a certain speed is reached
CarScene.prototype.launchCountdown = function() {
  var _this = this;
  _this.flagCountdown = true;
  _this.countDown = setTimeout(function(){
    TweenLite.to(_this.car.instance.position, 1.5, {x:10});
    TweenLite.to(_this.carShadow.instance.position, 1.5, {x:10});
   
     setTimeout(function(){
      window.summer.nextCanvas();
     },2000)
  }.bind(_this),2000);
};

// Cancel countdwon if car decelerate before end of it
CarScene.prototype.cancelCountdown = function() {
  var _this = this;
  _this.flagCountdown = false;
  clearTimeout(_this.countDown);
};