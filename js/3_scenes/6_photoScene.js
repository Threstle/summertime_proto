var PhotoScene = function(canvas){
	var _this = this;

	ThreeScene.call(_this,canvas);
  _this.cameraMove = false;
	_this.cameraPan = false;
  _this.isZooming = false;
  _this.ecart = {x:0,y:100};
  _this.limitCam.yMin = 0.27;

  var ambientLight = new THREE.AmbientLight(0x5673ff);
  _this.renderer.setClearColor(0xe2f5fb,1);


 
 _this.backZoom = new Background(_this.scene,_this.objects,{x:6,y:0},{x:0,y:100,z:0},{x:25,y:25,z:1},{x:0,y:0,z:0},{texture:'dist/img/5_PHOTO_zoom-fond.png'});
 _this.bateau = new Background(_this.scene,_this.objects,{x:0,y:0},{x:3,y:96,z:0},{x:2.5,y:2.5,z:1},{x:0,y:0,z:0},{texture:'dist/img/5_PHOTO_bateau.png'});
 _this.bateau = new Background(_this.scene,_this.objects,{x:0,y:0},{x:3,y:101,z:0},{x:2.6,y:1,z:1},{x:0,y:0,z:0},{texture:'dist/img/5_PHOTO_avion.png'});
 
 _this.sol = new Collada(_this.scene,_this.objects,'src/3Dobj/panorama/sol/',{x:0,y:-2,z:5},0.035,{x:0,y:0,z:0},{});
 _this.barriere = new Collada(_this.scene,_this.objects,'src/3Dobj/panorama/barriere/',{x:0,y:-1.8,z:5},0.035,{x:0,y:-Math.PI/2,z:0},{});

 _this.car = new Collada(_this.scene,_this.objects,'src/3Dobj/austin/',{x:-1,y:-2,z:1},0.035,{x:0,y:-Math.PI/6,z:0},{isAnimated:true});
 // _this.mask = new Background(_this.scene,_this.objects,{x:0,y:0},{x:0,y:0,z:0},{x:1.4,y:1.4,z:1},{x:0,y:0,z:0},{texture:'../textures/sea_mask.png'});
 _this.sun = new Background(_this.scene,_this.objects,{x:0,y:0},{x:-3,y:-3,z:-6},{x:18,y:18,z:1},{x:0,y:0,z:0},{texture:'dist/img/5_PHOTO_sun.png',ratio:{x:1,y:1}});
 _this.mer = new Background(_this.scene,_this.objects,{x:0,y:0},{x:0,y:-5.6,z:-5.5},{x:50,y:50,z:1},{x:0,y:0,z:0},{texture:'dist/img/5_PHOTO_mer.png',ratio:{x:1,y:1}})
 _this.nuage = new Background(_this.scene,_this.objects,{x:1,y:0},{x:0,y:-5.3,z:-5.2},{x:30,y:30,z:1},{x:0,y:0,z:0},{texture:'dist/img/5_PHOTO_nuages.png',ratio:{x:1,y:1}});
 _this.trainee = new Background(_this.scene,_this.objects,{x:0,y:0},{x:0,y:-5.3,z:-5.1},{x:30,y:30,z:1},{x:0,y:0,z:0},{texture:'dist/img/5_PHOTO_trainee.png',ratio:{x:1,y:1}});
  
 _this.mountains = new Background(_this.scene,_this.objects,{x:0,y:0},{x:0,y:-1.3,z:-5},{x:30,y:30,z:1},{x:0,y:0,z:0},{texture:'dist/img/5_PHOTO_mountains.png',ratio:{x:1,y:1}});
 _this.perso = new Background(_this.scene,_this.objects,{x:0,y:0},{x:-0.1,y:-1.1,z:2},{x:1.3,y:1.9,z:1},{x:0,y:0,z:0},{texture:'dist/img/5_PHOTO_perso.png',ratio:{x:1,y:1}});
 
 //_this.mask.instance.visible = false;

 
 
};
PhotoScene.prototype = Object.create(ThreeScene.prototype);
PhotoScene.prototype.constructor = PhotoScene;

// UGLY /!\ Rewriting of the ThreeScene.moveCamera method. Does more or less the same thing, but can also pan the zoom view. Not the best way to do it. I'll rewrite it.
PhotoScene.prototype.moveCamera = function() {
  var _this = this;

  if(_this.mouse){
    var xDistance = 0;
    var yDistance = 0;
    var distance = 0;
    var mouseCam = {x:_this.mouse.x,y:_this.mouse.y};

    if(_this.isLeaving)mouseCam = {x:0,y:0};


    if(_this.isZooming){
      xDistance = ((-mouseCam.x*9) - _this.camera.position.x)+_this.ecart.x;
      yDistance = ((-mouseCam.y*9) - _this.camera.position.y)+_this.ecart.y;
      distance = Math.sqrt(xDistance * xDistance + yDistance * yDistance);

      _this.camera.position.x += xDistance * 0.15;
      _this.camera.position.y += yDistance * 0.15;
    }
    else{

      if(mouseCam.x > _this.limitCam.xMax)mouseCam.x = _this.limitCam.xMax;
      if(mouseCam.y > _this.limitCam.yMax)mouseCam.y = _this.limitCam.yMax;
      if(mouseCam.x < _this.limitCam.xMin)mouseCam.x = _this.limitCam.xMin;
      if(mouseCam.y < _this.limitCam.yMin)mouseCam.y = _this.limitCam.yMin;

      xDistance = (mouseCam.x - _this.camera.position.x);
      yDistance = (mouseCam.y - _this.camera.position.y);
      distance = Math.sqrt(xDistance * xDistance + yDistance * yDistance);

      _this.camera.position.x += xDistance * 0.15;
      _this.camera.position.y += yDistance * 0.15;
      _this.camera.lookAt(new THREE.Vector3(0,0,0));
      
    }
   }
};

PhotoScene.prototype.setUI = function() {
    var _this = this;

    _this.ui = document.createElement('div');
    _this.ui.id ="ui-photo";
    _this.ui.className = "ui";
    _this.wrapper.appendChild(_this.ui);
    $(_this.ui).html('<img id="jumelles" src="'+Const.IMGS+'5_PHOTO_sea_mask.png"></img>');
    _this.jumelles = $('#jumelles');
};

// Focus the camera on the sprite of the sea and add binoculars mask
PhotoScene.prototype.zoom = function() {
  var _this = this;
  _this.camera.rotation.set(0,0,0);
  _this.camera.position.set(0,_this.ecart.y,_this.camera.position.z);
  _this.cameraMove = true;
  _this.cameraPan = true;
  _this.isZooming = true;
  _this.jumelles.css('display','block');

};

// Focus the camera on the photo scene and remove binoculars mask
PhotoScene.prototype.deZoom = function() {
  var _this = this;
  _this.camera.position.set(0,0,_this.camera.position.z);
  _this.cameraMove = false;
  _this.cameraPan = false;
  _this.isZooming = false;
  _this.jumelles.css('display','none');
};

PhotoScene.prototype.update = function() {
    var _this = this;
    if(_this.isLeaving){
      _this.moveCamera();
    }
     
    if(_this.nuage)_this.nuage.ratio = 0.000001;
  
        
  
};

PhotoScene.prototype.keyPress = function(e) {};

PhotoScene.prototype.keyBegin = function(e) {
  var _this = this;
  if(!_this.isZooming)_this.zoom();
};

PhotoScene.prototype.keyStop = function(e) {
  var _this = this;
  _this.deZoom();
};


