var ParasolScene = function(canvas){
	
	ThreeScene.call(this,canvas);
  this.cameraMove = true;
  this.cameraPan = true;
  this.camera.position.z = 8;
  this.renderer.setClearColor( 0xfce2a1, 1 );
  this.mouseRatio = 0.3;

    this.ground = new Background(this.scene,this.objects,{x:0,y:0},{x:0,y:0,z:4},{x:14,y:14,z:1},{x:0,y:0,z:0},{texture:'dist/img/6_BEACH_beach_floor.jpg',ratio:{x:1,y:1}});
    this.ground = new Background(this.scene,this.objects,{x:0,y:0},{x:0,y:0,z:4},{x:11,y:11,z:1},{x:0,y:0,z:0},{texture:'dist/img/6_BEACH_beach_floor_shadows.png',ratio:{x:1,y:1}});
    this.parasol = new Collada(this.scene,this.objects,'src/3Dobj/beach/parasol/',{x:1.8,y:0.7,z:3.7},0.017,{x:-Math.PI/2,y:-Math.PI,z:-Math.PI});
    this.sac = new Collada(this.scene,this.objects,'src/3Dobj/beach/bag/',{x:0.65,y:-0.29,z:4},0.018,{x:Math.PI/2,y:-Math.PI/12,z:0});
    this.photoCamera = new Collada(this.scene,this.objects,'src/3Dobj/suitcase/camera/',{x:0.09,y:0.67,z:4},0.035,{x:Math.PI/2,y:Math.PI/2.7,z:0});
    this.jumelles = new Collada(this.scene,this.objects,'src/3Dobj/suitcase/jumelles/',{x:-0.3,y:0.10,z:4.0205},0.029,{x:-3.14/2,y:3.4,z:0});
};
ParasolScene.prototype = Object.create(ThreeScene.prototype);
ParasolScene.prototype.constructor = ParasolScene;
