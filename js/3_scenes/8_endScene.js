var EndScene = function(canvas){
    ThreeScene.call(this,canvas);
   



};
EndScene.prototype = Object.create(ThreeScene.prototype);
EndScene.prototype.constructor = EndScene;


EndScene.prototype.setUI = function() {

    this.ui = document.createElement('div');
    this.ui.id ="ui-end";
    this.ui.className = "ui";
    this.wrapper.appendChild(this.ui);

    $(this.ui).html('<div id="intro-clouds"></div>'+
                                     '<div id="end-sun"></div>'+
                                     '<div id="intro-text">'+
                                        '<h1 class="">LES BEAUX MOMENTS SONT ENCORE MEILLEURS PARTAGÉS</h1>'+
                                     '</div>'+
                                     '<div id="partageBox">'+
                                        '<h3>PARTAGEZ VOTRE EXPÉRIENCE AVEC VOS AMIS</h3>'+
                                        '<ul>'+
                                        '<li><img src="dist/img/11_END_mail-share.png"></img></li>'+
                                        '<li><img src="dist/img/11_END_fb-share.png"></img></li>'+
                                        '<li><img src="dist/img/11_END_twitter-share.png"></img></li>'+
                                        '</ul>'+
                                     '</div>'
                                 );
    
};

EndScene.prototype.updateUI = function() {

};
