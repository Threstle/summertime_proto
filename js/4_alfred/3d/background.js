var background = function( exports )
{
    exports.init = function()
    {

        var mat = materials.bitmap.clone();
        textures.loadTexture( "src/3d/textures/3_depart/2048_background.jpg", function(t)
        {
            mat.uniforms.texture.value = t;
        });

        var geom = new THREE.BufferGeometry();
        geom.addAttribute( 'position', new THREE.BufferAttribute( new Float32Array([-.5, -.5,0,.5, -.5,0,.5,.5,0, -.5, -.5, 0,.5,.5, 0, -.5,.5, 0]), 3 ) );
        geom.addAttribute( 'uv', new THREE.BufferAttribute( new Float32Array([   0,0, 1,0, 1,1,     0,0, 1,1, 0,1     ] ), 2 ) );

        var mesh = new THREE.Mesh( geom, mat  );

        //mesh.position.z = -200;//.multiplyScalar( 256 );
        mesh.position.z = -1000;
        mesh.scale.set( stage3d.resolution.x, stage3d.resolution.y / 2, 1 );
        mesh.renderOrder = -1;
        exports.mesh = mesh;

    };



    return exports;

}({});