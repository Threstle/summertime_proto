// var Summertime = function(){

// 	// setup physic engine
// 	// /!\ BUG TO REMOVE : WORKER DUPLICATES ITSELF AND TAKES A HUGE TOLL ON PERF.
// 	Physijs.scripts.worker = '/bower_components/Physijs/physijs_worker.js';
//     Physijs.scripts.ammo = '/bower_components/ammo.js/builds/ammo.js';

// 	this.mouse = new Mouse(document,this);
// 	this.key = new Keyboard(document,this);

// 	this.direction = 1;
// 	this.scenes = ['photoBack','laptop','suitcase','car','photo','parasol','end'];

// 	this.preventEvents = true;


// 	this.currentSceneIndex = 0;

// 	// add first scene in the queue
// 	this.addScene(this.scenes[this.currentSceneIndex]);

// 	window.onwheel = function(){ return false; };

// };

// // Take a string and add a scene to the dom based on it.
// Summertime.prototype.addScene = function(name) {
// 	switch(name){
// 		case("laptop"):
// 			this.currentScene = new LaptopScene(document.body);
// 			this.currentScene.grad = document.createElement('div');
// 			this.currentScene.grad.id ="lapTopGrad";
// 			this.currentScene.grad.className = "grad";
// 			document.body.appendChild(this.currentScene.grad);
// 			break;
// 		case("suitcase"):
// 			this.currentScene = new SuitCaseScene(document.body);
// 			this.currentScene.grad = document.createElement('div');
// 			this.currentScene.grad.id ="suitCaseGrad";
// 			this.currentScene.grad.className = "grad";
// 			document.body.appendChild(this.currentScene.grad);
// 			break;
// 		case("car"):
// 			this.currentScene = new CarScene(document.body);
// 			this.currentScene.grad = document.createElement('div');
// 			this.currentScene.grad.id ="carGrad";
// 			this.currentScene.grad.className = "grad";
// 			document.body.appendChild(this.currentScene.grad);
// 			break;
// 		case("photo"):
// 			this.currentScene = new PhotoScene(document.body);
// 			this.currentScene.grad = document.createElement('div');
// 			this.currentScene.grad.id ="photoGrad";
// 			this.currentScene.grad.className = "grad";
// 			document.body.appendChild(this.currentScene.grad);
// 			break;
// 		case("parasol"):
// 			this.currentScene = new ParasolScene(document.body);
// 			this.currentScene.grad = document.createElement('div');
// 			this.currentScene.grad.id ="parasolGrad";
// 			this.currentScene.grad.className = "grad";
// 			document.body.appendChild(this.currentScene.grad);
// 			break;
// 		case("photoBack"):
// 			this.currentScene = new PhotoSceneBack(document.body);
// 			this.currentScene.grad = document.createElement('div');
// 			this.currentScene.grad.id ="photoGradBack";
// 			this.currentScene.grad.className = "grad";
// 			document.body.appendChild(this.currentScene.grad);
// 			break;
// 		case("intro"):
// 			this.currentScene = new IntroScene(document.body);
// 			this.currentScene.grad = document.createElement('div');
// 			this.currentScene.grad.id ="introGrad";
// 			this.currentScene.grad.className = "grad";
// 			document.body.appendChild(this.currentScene.grad);
// 		break;
// 		case("end"):
// 			this.currentScene = new EndScene(document.body);
// 			this.currentScene.grad = document.createElement('div');
// 			this.currentScene.grad.id ="endGrad";
// 			this.currentScene.grad.className = "grad";
// 			document.body.appendChild(this.currentScene.grad);
// 		break;
// 		default: break;
// 	}
	
// };

// // Scroll to scene in 1st parameter and remove the scene in 2nd parameter from the dom
// Summertime.prototype.scrollToScene = function(scene,oldScene) {
// 	$('html, body').delay(300).animate({
//         scrollTop: $(scene.renderer.domElement).offset().top
//     }, 2000,function(){

//     	if(oldScene){
    			
//     		// remove old scene from dom
//     		document.body.removeChild(oldScene.wrapper);
//     		oldScene.scene = null;
//     		oldScene.renderer = null;
//     		window.removeEventListener('resize',oldScene.resizeCallback);
//     		cancelAnimationFrame(oldScene.requestId);
//     		document.body.removeChild(oldScene.grad);
    	
//     		$('html, body').animate({scrollTop: $(scene.renderer.domElement).offset().top}, 0);
    		
//     		oldScene = null;
//     	}
//     });

// };


// // Increment currentSceneIndex and change scene.
// Summertime.prototype.nextCanvas = function() {
// 	//this.currentScene.fade();
// 	this.currentScene.isLeaving = true;
// 	var oldScene = this.currentScene;

// 	if(this.currentSceneIndex+this.direction>this.scenes.length-1){
// 		this.direction = -1;
// 	}
// 	if(this.currentSceneIndex+this.direction<0){
// 		this.direction = 1;
// 	}

// 	this.currentSceneIndex+=this.direction;

// 	this.addScene(this.scenes[this.currentSceneIndex]);
// 	setTimeout(function(){
// 		this.scrollToScene(this.currentScene,oldScene);
// 	}.bind(this),5000)
	
	
// };

// // Propagate the drag event to the current scene
// Summertime.prototype.propagateDrag = function(amountX,amountY){
// 	if(this.currentScene)this.currentScene.drag(amountX,amountY);
// };

// // Propagate the endDrag event to the current scene
// Summertime.prototype.endDrag = function() {
// 	this.currentScene.isDrag = false;
// 	this.currentScene.endDrag();
// };

// // Propagate the mouseMove event to the current scene
// Summertime.prototype.propagateMouseMove = function(e){
// 	if(this.preventEvents){
// 		e.preventDefault();
//         e.stopPropagation();
//     }
// 	if(this.currentScene)this.currentScene.propagateMouseMove(e);
// };

// // Propagate the keyDown event to the current scene
// Summertime.prototype.propagateKey = function(power,e){
// 	if(this.preventEvents && e){
// 		e.preventDefault();
//         e.stopPropagation();
//     }
// 	if(this.currentScene!=null)this.currentScene.loadKey(power,e);

// };

// // Propagate the keyPress event to the current scene
// Summertime.prototype.propagateKeyPress = function(e){
// 	if(this.preventEvents){
// 		e.preventDefault();
//         e.stopPropagation();
//     }
//     // next canvas if keyDown is pressed
//     if(e.keyCode==40)this.nextCanvas();
// 	else if(this.currentScene!=null)this.currentScene.keyPress(e);

// };

// Summertime.prototype.propagateKeyBegin = function(e){
// 	if(this.preventEvents){
// 		e.preventDefault();
//         e.stopPropagation();
//     }
// 	if(this.currentScene!=null)this.currentScene.keyBegin(e);

// };

// Summertime.prototype.propagateKeyStop = function(e){
// 	if(this.preventEvents){
// 		e.preventDefault();
//         e.stopPropagation();
//     }
// 	if(this.currentScene!=null)this.currentScene.keyStop(e);

// };

// // Launch app
// var summer = new Summertime();