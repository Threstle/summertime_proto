cd E:\work\151227_alfred\summertime_proto\src\3Dobj\

rem 1 desk


rem    python convert_obj_three.py -i obj\1_desk\laptop_top.obj       -o E:\work\151227_alfred\summertime_proto\src\3d\models\1_desk\laptop_top.js                -t binary
rem    python convert_obj_three.py -i obj\1_desk\pen0.obj             -o E:\work\151227_alfred\summertime_proto\src\3d\models\1_desk\pen0.js                      -t binary
rem    python convert_obj_three.py -i obj\1_desk\pen1.obj             -o E:\work\151227_alfred\summertime_proto\src\3d\models\1_desk\pen1.js                      -t binary
rem    python convert_obj_three.py -i obj\1_desk\pot.obj              -o E:\work\151227_alfred\summertime_proto\src\3d\models\1_desk\pot.js                       -t binary
    python convert_obj_three.py -i obj\1_desk\desk.obj            -o E:\work\151227_alfred\summertime_proto\src\3d\models\1_desk\desk.js                     -t binary


rem 2 bag


rem    python convert_obj_three.py -i obj\2_bag\Bed.obj                -o E:\work\151227_alfred\summertime_proto\src\3d\models\2_bag\bed.js                -t binary
rem    python convert_obj_three.py -i obj\2_bag\Camera.obj             -o E:\work\151227_alfred\summertime_proto\src\3d\models\2_bag\camera.js             -t binary
rem    python convert_obj_three.py -i obj\2_bag\Floor.obj              -o E:\work\151227_alfred\summertime_proto\src\3d\models\2_bag\floor.js              -t binary
rem    python convert_obj_three.py -i obj\2_bag\Handle_bag.obj         -o E:\work\151227_alfred\summertime_proto\src\3d\models\2_bag\handle.js             -t binary
rem    python convert_obj_three.py -i obj\2_bag\Jumelle.obj            -o E:\work\151227_alfred\summertime_proto\src\3d\models\2_bag\jumelle.js            -t binary
rem    python convert_obj_three.py -i obj\2_bag\shirt.obj              -o E:\work\151227_alfred\summertime_proto\src\3d\models\2_bag\shirt.js              -t binary
rem    python convert_obj_three.py -i obj\2_bag\Sport_bag.obj          -o E:\work\151227_alfred\summertime_proto\src\3d\models\2_bag\bag.js                -t binary
rem    python convert_obj_three.py -i obj\2_bag\bag_top.obj          -o E:\work\151227_alfred\summertime_proto\src\3d\models\2_bag\bag_top.js                -t binary


rem 3 depart


rem    python convert_obj_three.py -i obj\3_depart\car_body.obj                -o E:\work\151227_alfred\summertime_proto\src\3d\models\3_depart\car_body.js           -t binary
rem    python convert_obj_three.py -i obj\3_depart\car_wheels_back.obj         -o E:\work\151227_alfred\summertime_proto\src\3d\models\3_depart\car_wheels_back.js    -t binary
rem    python convert_obj_three.py -i obj\3_depart\car_wheels_front.obj        -o E:\work\151227_alfred\summertime_proto\src\3d\models\3_depart\car_wheels_front.js   -t binary
rem    python convert_obj_three.py -i obj\3_depart\suitcase_bottom.obj         -o E:\work\151227_alfred\summertime_proto\src\3d\models\3_depart\suitcase_bottom.js    -t binary
rem    python convert_obj_three.py -i obj\3_depart\suitcase_top.obj            -o E:\work\151227_alfred\summertime_proto\src\3d\models\3_depart\suitcase_top.js       -t binary
rem    python convert_obj_three.py -i obj\3_depart\road.obj                    -o E:\work\151227_alfred\summertime_proto\src\3d\models\3_depart\road.js               -t binary


rem 4 paysage

rem    python convert_obj_three.py -i obj\4_paysage\floor.obj                   -o E:\work\151227_alfred\summertime_proto\src\3d\models\4_paysage\floor.js               -t binary
rem    python convert_obj_three.py -i obj\4_paysage\garde_corps.obj             -o E:\work\151227_alfred\summertime_proto\src\3d\models\4_paysage\garde_corps.js         -t binary

rem 5 beach

rem    python convert_obj_three.py -i obj\5_beach\beach.obj        -o E:\work\151227_alfred\summertime_proto\src\3d\models\5_beach\beach.js        -t binary
rem    python convert_obj_three.py -i obj\5_beach\parasol.obj      -o E:\work\151227_alfred\summertime_proto\src\3d\models\5_beach\parasol.js      -t binary


rem 8 room

rem    python convert_obj_three.py -i obj\6_room\room.obj               -o E:\work\151227_alfred\summertime_proto\src\3d\models\6_room\room.js           -t binary


pause