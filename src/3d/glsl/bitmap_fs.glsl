
uniform sampler2D texture;
uniform float brightness;
uniform float saturation;
uniform float contrast;
uniform float alpha;
uniform vec2 resolution;
uniform float radius;
uniform vec3 color;


const vec3 luma = vec3( 0.299, 0.587, 0.114 );
vec3 csb( vec3 color, float brt, float sat, float con)
{
    vec3 brtColor       = color * brt;
    float intensityf    = dot( brtColor, vec3(0.2125,0.7154,0.0721) );
    vec3 satColor       = mix( vec3( intensityf ), brtColor, sat );
    return mix( vec3( .5 ), satColor, con);
}

varying vec2 vUv;
void main(){

    vec4 tex = texture2D( texture, vUv );
    vec3 rgb = csb( tex.rgb, brightness, saturation, contrast );

    vec2 p =  2. * ( gl_FragCoord.xy / resolution ) - 1.;
    p.x *= resolution.x / resolution.y;

    vec2 le = vec2( -.4, 0. );
    vec2 re = vec2(  .4, 0. );
    float d = max(  ( 1.-( distance( p, le ) * radius )  ) ,  ( 1.-( distance( p, re ) *radius )  ) );
    rgb = mix( color, rgb, smoothstep( 0.5, 0.55, d ) );

    gl_FragColor = vec4( rgb, smoothstep( 0.5, 1., tex.a ) * alpha );

}
