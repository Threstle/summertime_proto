
uniform vec2 deltaUv;
uniform vec2 scaleUv;
varying vec2 vUv;
void main()
{
    vUv = ( uv * scaleUv ) + deltaUv;
    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1. );
}