
uniform sampler2D texture;
uniform float frameCount;
uniform float transition;

varying vec2 vUv;
void main()
{


    float t = transition;

    float lastFrame = frameCount-1.;

    float id = min( lastFrame, floor( frameCount * t ) );

    float nextFrame = id + 1.;

    float delta = 1. / frameCount;
    float nt = ( t - ( id * delta ) ) / delta;

    vec2 uv0 = vUv + vec2( id * delta, 0. );
    vec2 uv1 = vUv + vec2( min( lastFrame * delta, nextFrame * delta ), 0. );

    gl_FragColor =  mix(  texture2D( texture, uv0 ),  texture2D( texture, uv1 ), nt );


}
