
uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D gradient;

uniform float alphaThreshold;
uniform float transition;
uniform float brightness;
uniform vec2 resolution;

uniform float angle;
uniform vec2 offset;
uniform vec2 p0;
uniform vec2 p1;

const float radius = 1.;
const vec2 cen = vec2( 0.5, 0.5 );
const vec3 luma = vec3( 0.299, 0.587, 0.114 );


vec3 bsc( vec3 color, float brt, float sat, float con)
{
    vec3 brtColor       = color * brt;
    float intensityf    = dot( brtColor, vec3(0.2125,0.7154,0.0721) );
    vec3 satColor       = mix( vec3( intensityf ), brtColor, sat );
    return mix( vec3( .5 ), satColor, con);
}



vec2 project( vec2 p, vec2 a, vec2 b ){

    float A = p.x - a.x;
    float B = p.y - a.y;
    float C = b.x - a.x;
    float D = b.y - a.y;
    float dot = A * C + B * D;
    float len = C * C + D * D;
    float t = dot / len;
    return vec2( a.x + t * C, a.y + t * D );
}
vec2 norm( vec2 p0, vec2 p1 ){
    return normalize(  vec2( -( p1.y - p0.y ), ( p1.x - p0.x ) ) ) * .5;
}

void main()
{

    //screen space uvs
    vec2 uv = gl_FragCoord.xy / resolution;

    //image gradient
    /*
    mat2 rot = mat2(cos( angle ), -sin( angle ),
                    sin( angle ),  cos( angle ));
    vec4 dis = texture2D( gradient, cen  + ( ( uv - offset ) / radius ) * rot );
    //*/
    vec4 dis = texture2D( gradient, uv );
    float grey = dot( dis.xyz, luma );
    float tg = smoothstep( grey - alphaThreshold,  grey + alphaThreshold, transition );


    vec2 n = norm(p0,p1);
    vec2 c = p0 + (p1 - p0 ) * .5;
    vec2 a = c - n;
    vec2 b = c + n;

    vec2 pp = project( uv, b, a ) - c;
    float d = pp.x - uv.x + pp.y - uv.y ;
    float s = sign( d );
    pp *= ( s * exp( d * transition ) );

    float len = ( length( pp ) / length( c ) );
    float t = smoothstep( len - alphaThreshold,  len + alphaThreshold, transition );

    //source
    vec4 tex0 = texture2D( texture0, uv );

    //dest
    vec4 tex1 = texture2D( texture1, uv );

    //alpha
    tex0.a = step( t, len ) + step( len, t );

    //switch
    vec4 tex = mix( tex0, tex1, t );

    float v = brightness * ( 1. - abs( sin( ( .5 + transition ) * 3.14159 ) ) );
    tex.rgb = bsc( tex.rgb, 1. + v * 5., 1., 1. );
    gl_FragColor = vec4( tex.rgb, tex.a );

}
