{

    "metadata" :
    {
        "formatVersion" : 3.1,
        "sourceFile"    : "road.obj",
        "generatedBy"   : "OBJConverter",
        "vertices"      : 16,
        "faces"         : 18,
        "normals"       : 0,
        "uvs"           : 16,
        "materials"     : 1
    },

    "materials": [	{
	"DbgColor" : 15658734,
	"DbgIndex" : 0,
	"DbgName" : "Floor"
	}],

    "buffers": "road.bin"

}
